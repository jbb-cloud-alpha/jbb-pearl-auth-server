package org.jbb.cloud.pearl;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

  @Override
  public void configure(HttpSecurity http) throws Exception {
    // @formatter:off
//    http
//        // Just for laughs, apply OAuth protection to only 2 resources
//        .requestMatcher(new NegatedRequestMatcher(new AntPathRequestMatcher("/health-check")))
//        .authorizeRequests()
//        .anyRequest().access("#oauth2.hasScope('read')");

    http.authorizeRequests()
        .antMatchers("/health-check", "/swagger-ui.html", "/swagger/v2/**", "/swagger-resources/**",
            "/", "/oauth/token").permitAll() // #4
        .antMatchers("/admin/**").access("#oauth2.hasScope('read')") // #6
        .anyRequest().authenticated() // 7
        .and()
        .formLogin()  // #8
        .loginProcessingUrl("/login") // #9
        .permitAll(); // #5
    // @formatter:on
  }

  @Override
  public void configure(ResourceServerSecurityConfigurer resources)
      throws Exception {
    resources.resourceId("sparklr");
  }
}

